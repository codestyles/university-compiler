from ply import lex

class Lexer:

    reserved = {
        'int': 'INTEGER',
        'float': 'FLOAT',
        'bool': 'BOOLEAN',
        'fun' : 'FUNCTION',
        'True': 'TRUE',
        'False': 'FALSE',
        'print': 'PRINT',
        'return': 'RETURN',
        'main': 'MAIN',
        'if': 'IF',
        'else': 'ELSE',
        'elseif': 'ELSEIF',
        'while': 'WHILE',
        'on' : 'ON',
        'where' : 'WHERE',
        'for': 'FOR',
        'and' : 'AND',
        'or' : 'OR',
        'not' : 'NOT',
        'in' : 'IN'
    }

    tokens = [
        'ID','INTEGERNUMBER','FLOATNUMBER',
        'ASSIGN','SUM','SUB','MUL','DIV','MOD',
        'GT','GE','LT','LE','EQ','NE',
        'LCB', 'RCB', 'LRB', 'RRB','LSB','RSB',
        'SEMICOLON','COLON', 'COMMA','ERROR'
    ]

    tokens += reserved.values()

    # COLONS
    t_SEMICOLON = r';'
    t_COLON = r':'
    t_COMMA = r','

    # BRACKETS
    t_LCB = r'\{'
    t_RCB = r'\}'
    t_LRB = r'\('
    t_RRB = r'\)'
    t_LSB = r'\['
    t_RSB = r'\]'

    # OPERATOR
    t_SUM = r'\+'
    t_SUB = r'\-'
    t_MUL = r'\*'
    t_DIV = r'\/'
    t_MOD = r'\%'
    t_GT = r'\>'
    t_GE = r'\>\='
    t_LT = r'\<'
    t_LE = r'\<\='
    t_EQ = r'\=\='
    t_NE = r'\!\='
    t_ASSIGN = r'\='


    def t_ERROR(self,t):
        r'(\+ | \- | \* | \/ | \% )+ (\ )* (\+ | \- | \* | \/ | \% )+ (\+ | \- | \* | \/ | \% | \ )*  | (\d{10,}\.\d+)|(\d+(\. \d+){2,})|(\d+\.^)|(\d{10,})|([0-9]+[a-zA-Z_][a-zA-Z_0-9]*)|([A-Z]+[a-zA-Z_0-9]*)'
        if t.value in self.reserved:
            t.type = self.reserved[t.value]
        return t

    def t_FLOATNUMBER(self, t):
        r'\d+\.\d+'
        t.value = float(t.value)
        return t


    def t_ID(self, t):
        r'[a-z_][a-zA-Z_0-9]*'
        if t.value in self.reserved:
            t.type = self.reserved[t.value]
        return t


    def t_INTEGERNUMBER(self, t):
        r'\d+'
        t.value = int(t.value)
        return t

    def t_newline(self, t):
        r'\n+'
        t.lexer.lineno += len(t.value)

    t_ignore = '\n \t'

    def t_error(self, t):
        raise Exception('Error at', t.value)
       

    def build(self, **kwargs):
        self.lexer = lex.lex(module=self, **kwargs)
        return self.lexer


