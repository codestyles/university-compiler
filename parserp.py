from ply import yacc
from lexer import Lexer
from nonTerminal import NonTerminal
from codeGenerator import CodeGenerator
import subprocess

class Parser:

    tokens= Lexer().tokens

    def __init__(self):
        self.codeGenerator = CodeGenerator()
        self.lastArrOffset = "0"
        self.lastArrSize = "0"
    
    def write_to_file(self, inpt):
        filew = open("output.c", "w")
        filew.write(inpt)
        filew.close()

    def run(self, inpt):
        self.write_to_file(inpt)
        print('=======================================')
        print('Compiling...')
        cmd = "output.c"
        subprocess.call(["gcc", cmd])
        print("\nOutput:\n")
        subprocess.call("./a.exe")
        print("")

    #program
    def p_program(self, p):
        "program : declist MAIN LRB RRB block"
        p[0] = NonTerminal()
        p[0].code = "#include <stdio.h>\n"
        p[0].code += "#include <setjmp.h>\n"
        p[0].code += "int array[(int)1e6];\n"
        p[0].code += "int {}".format(",".join(self.codeGenerator.ids))
        for i in range (0, self.codeGenerator.tempCount):
            p[0].code += ",T"+str(i)
        p[0].code += ";\nint main() {\n"
        p[0].code += p[1].code + p[5].code
        p[0].code += "}"
        self.codeGenerator.simbol_table()
        self.run(p[0].code)
        print("&&&&&&&&&&&&&\n"+ p[0].code)
        print("program : declist MAIN LRB RRB block")

    #declist
    # def p_declist_dec(self, p):
    #     "declist : dec"
    #     print("declist : dec")
    def p_declist_declistdec(self, p):
        "declist : declist dec"
        p[0] = NonTerminal()
        p[0].code = p[1].code + p[2].code
        print("declist : declist dec")
    def p_declist_lambda(self, p):
        "declist : "
        p[0] = NonTerminal()
        print("declist : ")


    #dec
    def p_dec_vardec(self, p):
        "dec : vardec"
        p[0] = NonTerminal()
        p[0].code = p[1].code
        print("dec : vardec")
    def p_dec_funcdec(self, p):
        "dec : funcdec"
        print("dec : funcdec")


    #type #TODO
    def p_type_int(self, p):
        "type : INTEGER"
        p[0] = p[1]
        print("type : INTEGER")
    def p_type_float(self, p):
        "type : FLOAT"
        p[0] = p[1]
        print("type : FLOAT")
    def p_type_bool(self, p):
        "type : BOOLEAN"
        p[0] = p[1]
        print("type : BOOLEAN")


    #iddec
    # def p_iddec_id(self, p):
    #     "iddec : ID"
    #     print("iddec : ID")
    # def p_iddec_array(self, p):
    #     "iddec : ID LSB exp RSB"
    #     print("iddec : ID LSB exp RSB")

    #TODO
    def p_iddec_lvalue(self, p):
        "iddec : lvalue"
        p[0] = p[1]
        print("iddec : lvalue")
    def p_iddec_assign(self, p):
        "iddec : lvalue ASSIGN exp"
        p[0] = NonTerminal()
        # p[1].code = p[1].place + " = " + p[3].place + ";\n"
        # p[0].code = p[3].code + p[1].code
        p[0].code = p[1].code + p[3].code
        p[0].code += p[1].place + " = " + str(p[3].get_managed_value()) + ";\n"
        p[0].place = p[1].place
        print("iddec : lvalue ASSIGN exp")

    #idlist #TODO
    def p_idlist_iddec(self, p):
        "idlist : iddec"
        p[0] = NonTerminal()
        p[0].code = p[1].code
        print("idlist : iddec")
    def p_idlist_idlist(self, p):
        "idlist : idlist COMMA iddec"
        p[0] = NonTerminal()
        p[0].code = p[1].code + p[3].code
        print("idlist : idlist COMMA iddec")


    #vardec
    def p_vardec(self, p):
        "vardec : idlist COLON type SEMICOLON"
        p[0] = NonTerminal()
        p[0].code = p[1].code
        print("vardec : idlist COLON type SEMICOLON")


    #funcdec
    def p_funcdc_type(self, p):
        "funcdec : FUNCTION ID LRB paramdecs RRB COLON type block"
        print("funcdec : FUNCTION ID LRB paramdecs RRB COLON type block")
    def p_funcdec_void(self, p):
        "funcdec : FUNCTION ID LRB paramdecs RRB block"
        print("funcdec : FUNCTION ID LRB paramdecs RRB block")

    #paramdecs
    def p_paramdecs_paramdeclist(self, p):
        "paramdecs : paramdecslist"
        print("paramdecs : paramdecslist")
    def p_paramdecs_lambda(self, p):
        "paramdecs : "
        print("paramdecs : ")

    #paramdecslist
    def p_paramdecslist_paramdec(self, p):
        "paramdecslist : paramdec"
        print("paramdecslist : paramdec")
    def p_paramdecslist_paramsdeclist(self, p):
        "paramdecslist : paramdecslist COMMA paramdec"
        print("paramdecslist : paramdecslist COMMA paramdec")

    #paramdec
    def p_paramdec(self, p):
        "paramdec : ID COLON type"
        print("paramdec : ID COLON type")
    def p_paramdec_array(self, p):
        "paramdec : ID LSB RSB COLON type"
        print("paramdec : ID LSB RSB COLON type")

    #block
    def p_block(self, p):
        "block : LCB stmtlist RCB"
        p[0] = NonTerminal()
        p[0].code = p[2].code
        print("block : LCB stmtlist RCB")
    # def p_block_lambda(self, p):
    #     "block : LCB RCB"
    #     print("block : LCB RCB")


    #stmtlist
    # def p_stmtlist_stmt(self, p):
    #     "stmtlist : stmt"
    #     print("stmtlist : stmt")
    def p_stmtlist_stmlist(self, p):
        "stmtlist : stmtlist stmt"
        p[0] = NonTerminal()
        p[0].code = p[1].code + p[2].code
        print("stmtlist : stmtlist stmt")
    def p_stmtlist_lambda(self, p):
        "stmtlist : "
        p[0] = NonTerminal()
        print("stmtlist : ")


    #lvalue    #TODO
    def p_lvalue(self, p):
        "lvalue : ID"
        p[0] = NonTerminal()
        p[0].place = p[1]
        if p[1] not in self.codeGenerator.ids:
            self.codeGenerator.ids.append(p[1])
        print("lvalue : ID")
    def p_lvalue_array(self, p):
        "lvalue : ID LSB exp RSB"
        p[0] = NonTerminal()
        p[0].code = p[3].code
        if p[1] not in self.codeGenerator.ids:
            p[0].code += p[1] + " = " + self.lastArrOffset + " + " + self.lastArrSize + ";\n"
            self.lastArrOffset = p[1]
            self.lastArrSize = str(p[3].get_managed_value())
            self.codeGenerator.ids.append(p[1])
        else:
            tmp = self.codeGenerator.generate_temp()
            p[0].code += tmp + " = " + p[1] + " + " + str(p[3].get_managed_value()) + ";\n"
            p[0].place = "array[{}]".format(tmp)
            # p[0].code += p[0].place + " = array[{}];\n".format(tmp)
        print("lvalue : ID LSB exp RSB")
    # def p_lvalue_lambda(self, p):
    #     "lvalue : "
    #     print("lvalue : ")

    #case
    def p_case_where(self, p):
        "case : WHERE const COLON stmtlist"
        print("case : WHERE const COLON stmtlist")
    
    #cases
    # def p_cases_case(self, p):
    #     "cases : case"
    #     print("cases : case")
    def p_cases_cases(self, p):
        "cases : cases case"
        print("cases : cases case")
    def p_cases_lambda(self, p):
        "cases : "
        print("cases : ")   


    #stmt
    def p_stmt_return(self, p):
        "stmt : RETURN exp SEMICOLON"
        p[0] = NonTerminal()
        print("stmt : RETURN exp SEMICOLON")
    def p_stmt_exp(self, p): #TODO
        "stmt : exp SEMICOLON"
        p[0] = NonTerminal()
        p[0].code = p[1].code
        print("##############", p[0].code)
        print("stmt : exp SEMICOLON")
    def p_stmt_block(self, p):
        "stmt : block"
        p[0] = NonTerminal()
        p[0].code = p[1].code
        print("stmt : block")
    def p_stmt_vardec(self, p):
        "stmt : vardec"
        p[0] = NonTerminal()
        p[0].code = p[1].code
        print("stmt : vardec")
    def p_stmt_while(self, p):
        "stmt : WHILE LRB exp RRB stmt"
        p[0] = NonTerminal()
        print("stmt : WHILE LRB exp RRB stmt")
    def p_stmt_on(self, p):
        "stmt : ON LRB exp RRB LCB cases RCB SEMICOLON"
        p[0] = NonTerminal()
        print("stmt : ON LRB exp RRB LCB cases RCB SEMICOLON")
    def p_stmt_for(self, p):
        "stmt : FOR LRB exp SEMICOLON exp SEMICOLON exp RRB stmt"
        p[0] = NonTerminal()
        print("stmt : FOR LRB exp SEMICOLON exp SEMICOLON exp RRB stmt")
    def p_stmt_for_range(self, p):
        "stmt : FOR LRB ID IN ID RRB stmt"
        p[0] = NonTerminal()
        print("stmt : FOR LRB ID IN ID RRB stmt")
    def p_stmt_if(self, p):
        "stmt : IF LRB exp RRB stmt elseiflist %prec IFF"
        p[0] = NonTerminal()
        print("stmt : IF LRB exp RRB stmt elseiflist")
    def p_stmt_if_else(self, p):
        "stmt : IF LRB exp RRB stmt elseiflist ELSE stmt"
        p[0] = NonTerminal()
        print("stmt : IF LRB exp RRB stmt elseiflist ELSE stmt")
    def p_stmt_print(self, p):
        "stmt : PRINT LRB ID RRB SEMICOLON"
        p[0] = NonTerminal()
        p[0].code = "printf(\"%d\",{});\n".format(p[3])
        print("stmt : PRINT LRB ID RRB SEMICOLON")


    #elseiflist
    # def p_elseiflist(self, p):
    #     "elseiflist : ELSEIF LRB exp RRB stmt"
    #     print("elseiflist : ELSEIF LRB exp RRB stmt")
    def p_elseiflist_elseiflist(self, p):
        "elseiflist : elseiflist ELSEIF LRB exp RRB stmt"
        p[0] = NonTerminal()
        print("elseiflist : elseiflist ELSEIF LRB exp RRB stmt")
    def p_elseiflist_lambda(self, p):
        "elseiflist : "
        p[0] = NonTerminal()
        print("elseiflist : ")

    #relopexp
    def p_relopexp(self, p):
        "relopexp  : exp relop exp %prec RE3"
        p[0] = NonTerminal()
        print("relopexp  : exp relop exp")
    # def p_relopexp_relopexp(self, p):
    #     "relopexp : relopexp relop exp %prec RE2"
    #     print("relopexp : relopexp relop exp")

    #exp #TODO
    def p_exp_lvalue(self, p):
        "exp : lvalue"
        p[0] = p[1]
        print("exp : lvalue")
    def p_exp_SUB(self, p):
        "exp : exp SUB exp"
        self.codeGenerator.generate_arithmetic_code(p)
        print("exp : exp SUB exp")

    def p_exp_SUM(self, p):
        "exp : exp SUM exp"
        self.codeGenerator.generate_arithmetic_code(p)
        print("exp : exp SUM exp")

    def p_exp_MUL(self, p):
        "exp : exp MUL exp"
        self.codeGenerator.generate_arithmetic_code(p)
        print("exp : exp MUL exp")

    def p_exp_DIV(self, p):
        "exp : exp DIV exp"
        self.codeGenerator.generate_arithmetic_code(p)
        print("exp : exp DIV exp")

    def p_exp_MOD(self, p):
        "exp : exp MOD exp"
        self.codeGenerator.generate_arithmetic_code(p)
        print("exp : exp MOD exp")

    def p_exp_relopexp(self, p):
        "exp : relopexp "
        print("exp : relopexp")
    def p_exp_lvalue_assign(self, p): #TODO
        "exp : lvalue ASSIGN exp"
        p[0] = NonTerminal()
        p[0].code = p[1].code + p[3].code
        p[0].code += p[1].place + " = " + str(p[3].get_managed_value()) + ";\n"
        p[0].place = p[1].place
        print("exp : lvalue ASSIGN exp")
    def p_exp_const(self, p):
        "exp : const"
        p[0] = p[1]
        print("exp : const")
    def p_exp_explist(self, p):
        "exp : ID LRB explist RRB"
        print("exp : ID LRB explist RRB")
    def p_exp_id(self, p):
        "exp : ID LRB RRB"
        print("exp : ID LRB RRB")
    def p_exp_exp(self, p):
        "exp : LRB exp RRB"
        p[0] = p[2]
        print("exp : LRB exp RRB")
    def p_exp_not_exp(self, p):
        "exp : NOT exp %prec RE4"
        print("exp : NOT exp")
    def p_exp_sub_exp(self, p):
        "exp : SUB exp"
        pp = [p[0], NonTerminal(), p[1], p[2]]
        self.codeGenerator.generate_arithmetic_code(pp)
        p[0] = pp[0]
        print("exp : SUB exp")

    #operator
    def p_operator_or(self, p):
        "operator : OR"
        print("operator : OR")

    def p_operator_and(self, p):
        "operator : AND"
        print("operator : AND")

    # def p_operator_sum(self, p):
    #     "operator : SUM"
    #     p[0] = p[1]
    #     print("operator : SUM")

    # def p_operator_sub(self, p):
    #     "operator : SUB"
    #     p[0] = p[1]
    #     print("operator : SUB")

    # def p_operator_mul(self, p):
    #     "operator : MUL"
    #     p[0] = p[1]
    #     print("operator : MUL")

    # def p_operator_div(self, p):
    #     "operator : DIV"
    #     p[0] = p[1]
    #     print("operator : DIV")

    # def p_operator_mod(self, p):
    #     "operator : MOD"
    #     p[0] = p[1]
    #     print("operator : MOD")


    #const
    def p_const_intnumber(self, p):
        "const : INTEGERNUMBER"
        p[0] = NonTerminal()
        p[0].value = p[1]
        print("const : INTEGERNUMBER")

    def p_const_floatnumber(self, p):
        "const : FLOATNUMBER"
        p[0] = NonTerminal()
        p[0].value = int(p[1])
        print("const : FLOATNUMBER")

    def p_const_true(self, p):
        "const : TRUE"
        print("const : TRUE")

    def p_const_false(self, p):
        "const : FALSE"
        print("const : FALSE")


    #relop
    def p_relop_gt(self, p):
        "relop : GT"
        print("relop : GT")
    def p_relop_lt(self, p):
        "relop : LT"
        print("relop : LT")
    def p_relop_ne(self, p):
        "relop : NE"
        print("relop : NE")
    def p_relop_eq(self, p):
        "relop : EQ"
        print("relop : EQ")
    def p_relop_ge(self, p):
        "relop : LE"
        print("relop : LE")
    def p_relop_le(self, p):
        "relop : GE"
        print("relop : GE")
    

    #explist
    def p_explist_exp(self, p):
        "explist : exp"
        print("explist : exp")
    def p_explist_explist(self, p):
        "explist : explist COMMA exp"
        print("explist : explist COMMA exp")



    precedence = (
        ('right','ASSIGN'),
        ('nonassoc', 'IFF'),
        ('nonassoc', 'ELSE','ELSEIF'),
        ('nonassoc','GT','LT','NE','EQ','LE','GE'),
        ('nonassoc', 'RE4'),
        ('nonassoc','RE3'),
        ('left', 'OR'),
        ('left', 'AND'),
        ('left', 'SUM', 'SUB'),
        ('left', 'MUL', 'DIV','MOD'),
        ('left', 'LSB','LRB','LCB'),
        
    )


    def p_error(self, p):
        print("Syntax error in input! ",p)
        #print(p.value)
        #raise Exception('ParsingError: invalid grammar at ', p)


    def build(self, **kwargs):
        self.pars = yacc.yacc(module=self, **kwargs)
        return self.pars