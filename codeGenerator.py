from nonTerminal import NonTerminal

class CodeGenerator:

        
    def __init__(self):
        self.ids = []
        self.tempCount = 0

    def generate_arithmetic_code(self, p):
        p[0] = NonTerminal()
        p[0].place = self.generate_temp()
        p[0].code = p[1].code + p[3].code
        if "array" in p[1].place and "array" in p[3].place:
            tmp = self.generate_temp()
            tmp2 = self.generate_temp()
            p[0].code += str(tmp) + " = " + str(p[1].get_managed_value()) + ";\n"
            p[0].code += str(tmp2) + " = " + str(p[3].get_managed_value()) + ";\n"
            p[0].code += str(p[0].place) + " = " + str(tmp) + " " + str(p[2]) + " " + str(tmp2) + ";\n"
        elif "array" in p[1].place:
            tmp = self.generate_temp()
            p[0].code += str(tmp) + " = " + str(p[1].get_managed_value()) + ";\n"
            p[0].code += str(p[0].place) + " = " + str(tmp) + " " + str(p[2]) + " " + str(p[3].get_managed_value()) + ";\n"
        elif "array" in p[3].place:
            tmp = self.generate_temp()
            p[0].code += str(tmp) + " = " + str(p[3].get_managed_value()) + ";\n"
            p[0].code += str(p[0].place) + " = " + str(p[1].get_managed_value()) + " " + str(p[2]) + " " + str(tmp) + ";\n"
        else:
            p[0].code += str(p[0].place) + " = " + str(p[1].get_managed_value()) + " " + str(p[2]) + " " + str(p[3].get_managed_value()) + ";\n"
        print("||||||||||", p[0].code)

    def generate_temp(self):
        temp = "T" + str(self.tempCount)
        self.tempCount += 1
        return temp

    def simbol_table(self):
        print("name\ttype\tsize\taddress")
        print("---------------------------------------------")
        for i in range(len(self.ids)):
            print("{}\tint\t4\t{}".format(self.ids[i], i * 4))